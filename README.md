# SDK for Fake Payment provider #

This repository contains SDK libraries to be used by client driven by .net core 2.2 or above

## What is this repository for? ##

* Source code for SDK
* Sample applications

## How do I get set up? ##

* execute "bash run-mvc-demo.sh"
    * it will restore nuget packages(including local)
    * build app
    * start self hosted app
* Navigate to https://localhost:5001

## SDK description ##
### __FakeSDK__ ###
> __Dependes on__:
> * Serilog
> * Newtonsoft.Json

> __Description__:
> Implements all neccessary communication with dumdumpay payment system

### __FakeSDK.Api__ ###
> __Dependes on__:
> * Serilog
> * RazorEngine.NetCore

> __Description__:
> Currently implements 2 semi-automated ways to obtain either rendered html to approve payment or get html to enter verification code and secure payment.
Created to be used by scenarios like : backend API + SPA client where backend can retrieve secure html to be displayed on web page by [iFrame/InnerHtml directive in angular/etc]

### __FakeSDK.Console__(_Postponed due to complexity and time consuming reasons_) ###
> __Dependes on__:
> * Serilog
> * HtmlAgilityPack

> __Description__:
> Tend to be used from console applications driven by State pattern. Where all information obtained from beggining and new request with delegate to retrieve verification code created. Then library handles all communications, form submition, parsing response html to prepare confirmation form. SDK would use HttpListener to handle verification callback and info would be returned to the client

