using System;
using System.Threading;
using System.Threading.Tasks;
using FakeSDK.Models;
using FakeSDK.MVCSample.ViewModels;
using FakeSDK.Services;
using Microsoft.AspNetCore.Mvc;

namespace FakeSDK.MVCSample.Controllers
{
    public class PaymentController : Controller
    {
        private readonly IFakePaymentService _fakePaymentService;

        public PaymentController(IFakePaymentServiceBuilder serviceBuilder)
        {
            _fakePaymentService = serviceBuilder.Build();
        }

        // GET
        public IActionResult Create()
        {
            var demoRequest = new CreatePaymentViewModel
            {
                Amount = 12312,
                CardHolder = "TEST TESTER",
                CardExpiryDate = DateTime.ParseExact("1123", "MMyy", null),
                CardNumber = "4111111111111111",
                Country = "CY",
                Currency = "USD",
                CVV = "111",
                Md = "testorder-1"
            };
            return View(demoRequest);
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreatePaymentViewModel request)
        {
            if(!ModelState.IsValid)
            {
                return View(request);
            }

            var response = await _fakePaymentService.CreatePaymentAsync(request.ToRequest(), CancellationToken.None);

            return RedirectToAction("Secure", new {request.Md, response.PaReq, response.TransactionId});
        }

        // GET
        public IActionResult Secure(string md, string paReq, string transactionId)
        {
            var absoluteCallbackUrl = $"http://localhost:5000/payment/secured/{transactionId}/";
            var demoRequest = new ConfirmPaymentViewModel
            {
                PaReq = paReq,
                Md = md,
                TermUrl = absoluteCallbackUrl
            };
            return View(demoRequest);
        }

        [HttpGet]
        public async Task<IActionResult> Secured(string id, [FromQuery]string md, [FromQuery]string paRes)
        {
            var demoRequest = new ConfirmPaymentRequest
            {
                PaRes = paRes,
                TransactionId = Guid.Parse(id)
            };
            var res = await _fakePaymentService.ConfirmPaymentAsync(demoRequest, CancellationToken.None);
            return RedirectToAction("Status", new {id});
        }

        public async Task<IActionResult> Status(Guid id)
        {
            var res = await _fakePaymentService.GetPaymentStatusAsync(id, CancellationToken.None);
            return View(res.Result);
        }
    }
}