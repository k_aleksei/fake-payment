using System;
using System.ComponentModel.DataAnnotations;
using FakeSDK.Models;

namespace FakeSDK.MVCSample.ViewModels
{
    public class CreatePaymentViewModel
    {
        [Required]
        [Range(0.0, 999_999.00, ErrorMessage = "Amount must be a valid number")]
        public double Amount { get; set; }

        [Required]
        [StringLength(3, MinimumLength = 3, ErrorMessage = "Should be valid currency code")]
        public string Currency { get; set; }

        [Required]
        [StringLength(2, MinimumLength = 2, ErrorMessage = "Should be valid country code")]
        public string Country { get; set; }

        [Required]
        public string CardNumber { get; set; }

        [Required]
        public string CardHolder { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime CardExpiryDate { get; set; }

        [Required]
        [StringLength(3, MinimumLength = 3, ErrorMessage = "Should be valid CVV code")]
        public string CVV { get; set; }

        [Required]
        public string Md { get; set; }

        public CreatePaymentRequest ToRequest()
        {
            return new CreatePaymentRequest
            {
                Amount = (uint) Amount * 100,
                CardNumber = CardNumber,
                Country = Country,
                Currency = Currency,
                CardHolder = CardHolder,
                CardExpiryDate = CardExpiryDate,
                CVV = CVV,
                OrderId = Guid.NewGuid().ToString()
            };
        }
    }
}