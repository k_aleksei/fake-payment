namespace FakeSDK.MVCSample.ViewModels
{
    public class ConfirmPaymentViewModel
    {
        public string Md { get; set; }
        public string PaReq { get; set; }
        public string TermUrl { get; set; }
    }
}