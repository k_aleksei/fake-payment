using System;

namespace FakeSDK.Console.Common
{
    internal static class Constants
    {
        public static string PaymentCreationHtmlTemplateName => "CreatePaymentTemplate.html";
        public static string LocalServerAddress = "http://localhost:8080/payment/confirmed";
        public static int CallbackAwaitingTimeoutSeconds = 30;
    }
}