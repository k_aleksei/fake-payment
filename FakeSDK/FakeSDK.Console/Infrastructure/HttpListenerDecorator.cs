using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FakeSDK.Console.Common;

namespace FakeSDK.Console.Infrastructure
{
    public class HttpListenerDecorator : IHttpListenerDecorator
    {
        private readonly HttpListener _listener;
        private readonly ManualResetEventSlim _manualResetEventSlim = new ManualResetEventSlim(false);
        private HttpListenerRequest _request;

        public HttpListenerDecorator()
        {
            _listener = new HttpListener
            {
                Prefixes = {Constants.LocalServerAddress}
            };
        }


        public void Dispose()
        {
            _listener.Stop();
            _manualResetEventSlim.Dispose();
        }

        public Task<bool> TryWaitCallbackOrTimeoutAsync(out string requestBody)
        {
            _listener.Start();
            _listener.BeginGetContext(OnRequestReceived, null);
            
            _manualResetEventSlim.Wait(TimeSpan.FromSeconds(Constants.CallbackAwaitingTimeoutSeconds));
            
            if(_request == null)
            {
                requestBody = null;
                return Task.FromResult(false);
            }

            using var stream = _request.InputStream;
            using var reader = new StreamReader(_request.InputStream);
            requestBody = reader.ReadToEnd();
            return Task.FromResult(true);
        }

        private void OnRequestReceived(IAsyncResult result)
        {
            var requestContext = _listener.EndGetContext(result);
            _request = requestContext.Request;
            RespondOnIncomingRequest(requestContext);
            _manualResetEventSlim.Set();
        }

        private static void RespondOnIncomingRequest(HttpListenerContext requestContext)
        {
            requestContext.Response.Headers.Clear();
            requestContext.Response.SendChunked = false;
            requestContext.Response.StatusCode = 200;
            requestContext.Response.Headers.Add("Server", string.Empty);
            requestContext.Response.Headers.Add("Date", string.Empty);
            requestContext.Response.Close();
        }
    }
}