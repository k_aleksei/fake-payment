using System;
using System.Threading.Tasks;

namespace FakeSDK.Console.Infrastructure
{
    public interface IHttpListenerDecorator: IDisposable
    {
        Task<bool> TryWaitCallbackOrTimeoutAsync(out string requestBody);
    }
}