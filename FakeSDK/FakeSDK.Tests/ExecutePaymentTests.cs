using System;
using System.Threading;
using System.Threading.Tasks;
using FakeSDK.Models;
using FakeSDK.Services;
using Newtonsoft.Json;
using NUnit.Framework;

namespace FakeSDK.Tests
{
    [TestFixture, Ignore("Made to be used only for development. Should no be run by continues deployment")]
    //[TestFixture]
    public class ExecutePaymentTests
    {
        [SetUp]
        public void SetUp()
        {
            _builder = new FakePaymentServiceBuilder().WithConfigurationForDemo();
        }

        [TearDown]
        public void Dispose()
        {
            _builder.Dispose();
        }

        private IFakePaymentServiceBuilder _builder;

        [Test]
        public async Task CreatePayment()
        {
            var createPaymentRequest = new CreatePaymentRequest
            {
                Amount = 12312,
                CardHolder = "TEST TESTER",
                CardExpiryDate = DateTime.ParseExact("1123", "MMyy", null),
                CardNumber = "4111111111111111",
                Country = "CY",
                Currency = "USD",
                OrderId = "DBB99946-A10A-4D1B-A742-577FA026BC01",
                CVV = "111"
            };

            var paymentService = _builder.Build();
            var createResponse = await paymentService.CreatePaymentAsync(createPaymentRequest, CancellationToken.None);

            Console.WriteLine(JsonConvert.SerializeObject(createResponse));
            Assert.NotNull(createResponse);
        }

        [Test]
        public async Task ConfirmPayment()
        {
            var confirmRequest = new ConfirmPaymentRequest
            {
                PaRes =
                    "ZWY1ZTg0MzktNjdlMC00NTY2LTg2NTItNGVlMzgzMTg5MTc5REJCOTk5NDYtQTEwQS00RDFCLUE3NDItNTc3RkEwMjZCQzAxZWY1ZTg0MzktNjdlMC00NTY2LTg2NTItNGVlMzgzMTg5MTc5",
                TransactionId = Guid.Parse("ef5e8439-67e0-4566-8652-4ee383189179")
            };

            var paymentService = _builder.Build();
            var response = await paymentService.ConfirmPaymentAsync(confirmRequest, CancellationToken.None);

            Console.WriteLine(JsonConvert.SerializeObject(response));
            Assert.NotNull(response);
        }

        [Test]
        public async Task GetConfirmationPageHtmlAsync()
        {
            var approvePaymentRequest = new AutoApprovePaymentRequest
            {
                Md = "tester-01",
                TransactionId = "ef5e8439-67e0-4566-8652-4ee383189179",
                CallbackUrl = "localhost"
            };

            var paymentService = _builder.Build();
            var response = await paymentService.GetConfirmationPageHtmlAsync(approvePaymentRequest, CancellationToken.None);

            Console.WriteLine(response);
            Assert.NotNull(response);
        }
    }
}