using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FakeSDK.Common;
using FakeSDK.Infrastructure;
using FakeSDK.Infrastructure.Exceptions;
using FakeSDK.Models;
using FakeSDK.Services;
using Moq;
using NUnit.Framework;

namespace FakeSDK.Tests.Services
{
    [TestFixture]
    public class FakePaymentServiceTests
    {
        [SetUp]
        public void SetUp()
        {
            _operationsCommunicatorMock = new Mock<IHttpCommunicator>();
            _approvalCommunicatorMock = new Mock<IHttpCommunicator>();
            var defaultResponse = new CreatePaymentResponse(default, default, default, default, default);
            _operationsCommunicatorMock
                .Setup(communicator => communicator.PostAsync<CreatePaymentRequest, CreatePaymentResponseInfo>(
                        It.IsAny<CreatePaymentRequest>(),
                        It.IsAny<string>()
                        , It.IsAny<CancellationToken>()))
                .ReturnsAsync(new CreatePaymentResponseInfo(defaultResponse));

            _httpCommunicatorFactoryMock = new Mock<IHttpCommunicatorFactory>();
            _httpCommunicatorFactoryMock
                .Setup(factory => factory.CreateOperationsCommunicator())
                .Returns(_operationsCommunicatorMock.Object);
            _httpCommunicatorFactoryMock
                .Setup(factory => factory.CreateApprovalCommunicator())
                .Returns(_approvalCommunicatorMock.Object);
        }

        private Mock<IHttpCommunicator> _operationsCommunicatorMock;
        private Mock<IHttpCommunicator> _approvalCommunicatorMock;
        private Mock<IHttpCommunicatorFactory> _httpCommunicatorFactoryMock;

        private FakePaymentService CreateService()
        {
            return new FakePaymentService(_httpCommunicatorFactoryMock.Object);
        }

        [Test]
        public void CreatesOperationCommunicatorInCtor()
        {
            CreateService();

            _httpCommunicatorFactoryMock.Verify(factory => factory.CreateOperationsCommunicator());
        }

        [Test]
        public void CreatesApprovalCommunicatorInCtor()
        {
            CreateService();

            _httpCommunicatorFactoryMock.Verify(factory => factory.CreateApprovalCommunicator());
        }

        [Test]
        public async Task SendsRequestToCommunicatorOnCreatePaymentAsync()
        {
            var request = new CreatePaymentRequest();

            await CreateService().CreatePaymentAsync(request, CancellationToken.None);

            _operationsCommunicatorMock
                .Verify(communicator => communicator.PostAsync<CreatePaymentRequest, CreatePaymentResponseInfo>(
                         request,
                         Constants.CreatePath,
                         CancellationToken.None));
        }

        [Test]
        public async Task ReturnsResultFromCommunicatorOnCreatePaymentAsync()
        {
            var request = new CreatePaymentRequest();
            var expectedResponse = new CreatePaymentResponse(default, default, default, default, default);
            _operationsCommunicatorMock
                .Setup(communicator => communicator.PostAsync<CreatePaymentRequest, CreatePaymentResponseInfo>(
                        It.IsAny<CreatePaymentRequest>(),
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>()))
                .ReturnsAsync(new CreatePaymentResponseInfo(expectedResponse));

            var result = await CreateService().CreatePaymentAsync(request, CancellationToken.None);

            Assert.AreSame(expectedResponse, result);
        }

        [Test]
        public void ReThrowsBadRequestExceptionIfCreationFailedOnCreatePaymentAsync()
        {
            var request = new CreatePaymentRequest();
            _operationsCommunicatorMock
                .Setup(communicator => communicator.PostAsync<CreatePaymentRequest, CreatePaymentResponseInfo>(
                        It.IsAny<CreatePaymentRequest>(),
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>()))
                .ThrowsAsync(new BadRequestException("{}"));

            Assert.ThrowsAsync<CreatePaymentException>(() => CreateService().CreatePaymentAsync(request, CancellationToken.None));
        }

        [Test]
        public async Task ThrowsCreatePaymentExceptionWithErrorDescriptionIfCreationFailedOnCreatePaymentAsync()
        {
            const string exceptionMessage = "CreatePaymentRequest failed";
            var request = new CreatePaymentRequest();
            _operationsCommunicatorMock
                .Setup(communicator => communicator.PostAsync<CreatePaymentRequest, CreatePaymentResponseInfo>(
                        It.IsAny<CreatePaymentRequest>(),
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>()))
                .ThrowsAsync(new Exception(exceptionMessage));

            try
            {
                await CreateService().CreatePaymentAsync(request, CancellationToken.None);
            }
            catch (CreatePaymentException e)
            {
                Assert.AreEqual(exceptionMessage, e.Response.Errors.Single().Message);
                Assert.AreEqual(Constants.Errors.CreateFailedErrorType, e.Response.Errors.Single().Type);
                return;
            }

            Assert.Fail("Expected exception was not thrown");
        }

        [Test]
        public async Task SendsRequestToCommunicatorOnConfirmPaymentAsync()
        {
            var request = new ConfirmPaymentRequest();

            await CreateService().ConfirmPaymentAsync(request, CancellationToken.None);

            _operationsCommunicatorMock.Verify(communicator =>
                                                   communicator.PostAsync<ConfirmPaymentRequest, ConfirmPaymentResponse>(
                                                    request,
                                                    Constants.ConfirmPath,
                                                    CancellationToken.None));
        }

        [Test]
        public async Task ReturnsResultFromCommunicatorOnConfirmPaymentAsync()
        {
            var request = new ConfirmPaymentRequest();
            var paymentInfo = new PaymentInfo(default, default, default, default, default, default);
            var expectedResponse = new ConfirmPaymentResponse(paymentInfo);
            _operationsCommunicatorMock
                .Setup(communicator => communicator.PostAsync<ConfirmPaymentRequest, ConfirmPaymentResponse>(
                        It.IsAny<ConfirmPaymentRequest>(),
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>()))
                .ReturnsAsync(expectedResponse);

            var result = await CreateService().ConfirmPaymentAsync(request, CancellationToken.None);

            Assert.AreSame(expectedResponse, result);
        }

        [Test]
        public void ReThrowsBadRequestExceptionIfCreationFailedOnConfirmPaymentAsync()
        {
            var request = new ConfirmPaymentRequest();
            _operationsCommunicatorMock
                .Setup(communicator => communicator.PostAsync<ConfirmPaymentRequest, ConfirmPaymentResponse>(
                        It.IsAny<ConfirmPaymentRequest>(),
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>()))
                .ThrowsAsync(new BadRequestException("{}"));

            Assert.ThrowsAsync<ConfirmPaymentException>(() => CreateService().ConfirmPaymentAsync(request, CancellationToken.None));
        }

        [Test]
        public async Task ThrowsConfirmPaymentExceptionWithErrorDescriptionIfCreationFailedOnConfirmPaymentAsync()
        {
            const string exceptionMessage = "ConfirmPaymentRequest failed";
            var request = new ConfirmPaymentRequest();
            _operationsCommunicatorMock
                .Setup(communicator => communicator.PostAsync<ConfirmPaymentRequest, ConfirmPaymentResponse>(
                        It.IsAny<ConfirmPaymentRequest>(),
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>()))
                .ThrowsAsync(new Exception(exceptionMessage));

            try
            {
                await CreateService().ConfirmPaymentAsync(request, CancellationToken.None);
            }
            catch (ConfirmPaymentException e)
            {
                Assert.AreEqual(exceptionMessage, e.Response.Errors.Single().Message);
                Assert.AreEqual(Constants.Errors.ConfirmFailedErrorType, e.Response.Errors.Single().Type);
                return;
            }

            Assert.Fail("Expected exception was not thrown");
        }

        [Test]
        public async Task SendsRequestToCommunicatorOnGetPaymentStatusAsync()
        {
            var transactionId = Guid.NewGuid();
            var expectedPath = string.Format(Constants.PaymentStatusPathTemplate, transactionId);

            await CreateService().GetPaymentStatusAsync(transactionId, CancellationToken.None);

            _operationsCommunicatorMock.Verify(communicator =>
                                                   communicator.GetAsync<PaymentStatusResponse>(expectedPath, CancellationToken.None));
        }

        [Test]
        public async Task ReturnsResultFromCommunicatorOnGetPaymentStatusAsync()
        {
            var paymentInfo = new PaymentInfo(default, default, default, default, default, default);
            var expectedResponse = new PaymentStatusResponse(paymentInfo);
            _operationsCommunicatorMock
                .Setup(communicator => communicator.GetAsync<PaymentStatusResponse>(It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(expectedResponse);

            var result = await CreateService().GetPaymentStatusAsync(Guid.Empty, CancellationToken.None);

            Assert.AreSame(expectedResponse, result);
        }

        [Test]
        public void ReThrowsBadRequestExceptionIfCreationFailedOnGetPaymentStatusAsync()
        {
            _operationsCommunicatorMock
                .Setup(communicator => communicator.GetAsync<PaymentStatusResponse>(It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .ThrowsAsync(new BadRequestException("{}"));

            Assert.ThrowsAsync<GetPaymentStatusException>(() => CreateService().GetPaymentStatusAsync(Guid.Empty, CancellationToken.None));
        }

        [Test]
        public async Task ThrowsGetPaymentStatusExceptionWithErrorDescriptionIfCreationFailedOnGetPaymentStatusAsync()
        {
            const string exceptionMessage = "failed";
            _operationsCommunicatorMock
                .Setup(communicator => communicator.GetAsync<PaymentStatusResponse>(It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .ThrowsAsync(new Exception(exceptionMessage));

            try
            {
                await CreateService().GetPaymentStatusAsync(Guid.Empty, CancellationToken.None);
            }
            catch (GetPaymentStatusException e)
            {
                Assert.AreEqual(exceptionMessage, e.Response.Errors.Single().Message);
                Assert.AreEqual(Constants.Errors.GetPaymentStatusFailedErrorType, e.Response.Errors.Single().Type);
                return;
            }

            Assert.Fail("Expected exception was not thrown");
        }

        [Test]
        public async Task SendsRequestToCommunicatorOnGetConfirmationPageHtmlAsync()
        {
            using var tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;
            const string callbackUrl = "localhost";
            var transactionId = Guid.NewGuid().ToString();
            const string md = "tester-test01";
            var request = new AutoApprovePaymentRequest
            {
                Md = md,
                CallbackUrl = callbackUrl,
                TransactionId = transactionId
            };

            await CreateService().GetConfirmationPageHtmlAsync(request, token);

            _approvalCommunicatorMock.Verify(c => c.SubmitFormToStringAsync(It.Is<Dictionary<string, string>>(d =>
                                                                                d[Constants.ApprovePaymentParameters.Md] == md &&
                                                                                d[Constants.ApprovePaymentParameters.TransactionId] ==
                                                                                transactionId &&
                                                                                d[Constants.ApprovePaymentParameters.CallbackUrl] ==
                                                                                callbackUrl),
                                                                            Constants.ApprovePaymentPath,
                                                                            token));
        }

        [TestCase("response")]
        [TestCase("another response")]
        public async Task ReturnsResultFromCommunicatorOnGetConfirmationPageHtmlAsync(string response)
        {
            _approvalCommunicatorMock
                .Setup(communicator => communicator.SubmitFormToStringAsync(It.IsAny<Dictionary<string,string>>(),It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(response);

            var result = await CreateService().GetConfirmationPageHtmlAsync(new AutoApprovePaymentRequest(), CancellationToken.None);

            Assert.AreSame(response, result);
        }

        [Test]
        public void ReThrowsBadRequestExceptionIfRequestFailedOnGetConfirmationPageHtmlAsync()
        {
            _approvalCommunicatorMock
                .Setup(communicator => communicator.SubmitFormToStringAsync(It.IsAny<Dictionary<string,string>>(),It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .ThrowsAsync(new BadRequestException("{}"));

            Assert.ThrowsAsync<AutoApprovePaymentException>(() => CreateService()
                                                                .GetConfirmationPageHtmlAsync(new AutoApprovePaymentRequest(), CancellationToken.None));
        }

        [Test]
        public async Task ThrowsGetPaymentStatusExceptionWithErrorDescriptionIfCreationFailedOnGetConfirmationPageHtmlAsync()
        {
            const string exceptionMessage = "failed";
            _approvalCommunicatorMock
                .Setup(communicator => communicator.SubmitFormToStringAsync(It.IsAny<Dictionary<string,string>>(),It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .ThrowsAsync(new Exception(exceptionMessage));

            try
            {
                await CreateService().GetConfirmationPageHtmlAsync(new AutoApprovePaymentRequest(), CancellationToken.None);
            }
            catch (AutoApprovePaymentException e)
            {
                Assert.AreEqual(exceptionMessage, e.Response.Errors.Single().Message);
                Assert.AreEqual(Constants.Errors.ApproveFailedErrorType, e.Response.Errors.Single().Type);
                return;
            }

            Assert.Fail("Expected exception was not thrown");
        }
    }
}