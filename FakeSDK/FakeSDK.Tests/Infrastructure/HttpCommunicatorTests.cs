using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using FakeSDK.Infrastructure;
using FakeSDK.Infrastructure.Exceptions;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;

namespace FakeSDK.Tests.Infrastructure
{
    [TestFixture]
    public class HttpCommunicatorTests
    {
        [SetUp]
        public void SetUp()
        {
            _httpClientFactoryMock = new Mock<IHttpClientFactory>();
            _httpClientMock = new Mock<IHttpClient>();
            _httpClientFactoryMock.Setup(factory => factory.GetOrCreate(It.IsAny<Uri>()))
                                  .Returns(_httpClientMock.Object);
            _httpClientMock.Setup(client => client.PostAsJsonAsync(It.IsAny<Request>(), It.IsAny<string>(), It.IsAny<CancellationToken>()))
                           .ReturnsAsync(new HttpResponseMessage {Content = new StringContent("{}")});
            _httpClientMock.Setup(client => client.GetAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
                           .ReturnsAsync(new HttpResponseMessage {Content = new StringContent("{}")});
            _httpClientMock
                .Setup(client => client.PostFormAsync(It.IsAny<Dictionary<string, string>>(),
                                                      It.IsAny<string>(),
                                                      It.IsAny<CancellationToken>()))
                .ReturnsAsync(new HttpResponseMessage {Content = new StringContent("{}")});
        }

        private readonly Uri _baseUri = new Uri("http://localhost");
        private Mock<IHttpClientFactory> _httpClientFactoryMock;
        private Mock<IHttpClient> _httpClientMock;

        private HttpCommunicator CreateCommunicator()
        {
            return new HttpCommunicator(_httpClientFactoryMock.Object, _baseUri);
        }

        [Test]
        public async Task CreateHttpClientWithCorrectBaseAddressOnPostAsync()
        {
            await CreateCommunicator().PostAsync<Request, Response>(new Request(), default, CancellationToken.None);

            _httpClientFactoryMock.Verify(factory => factory.GetOrCreate(_baseUri), Times.Once);
        }

        [TestCase("url/1")]
        [TestCase("url/2")]
        public async Task PassesAllParametersToHttpClientOnPostAsync(string path)
        {
            var request = new Request();

            await CreateCommunicator().PostAsync<Request, Response>(request, path, CancellationToken.None);

            _httpClientMock.Verify(client => client.PostAsJsonAsync(request, path, CancellationToken.None), Times.Once);
        }

        [TestCase("expected message 1")]
        [TestCase("another expected message")]
        public async Task ReturnsSerializedResponseOnPostAsync(string message)
        {
            var request = new Request();
            var response = new Response {Message = message};
            var json = JsonConvert.SerializeObject(response);
            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.Created)
            {
                Content = new StringContent(json)
            };
            _httpClientMock.Setup(client => client.PostAsJsonAsync(It.IsAny<Request>(), It.IsAny<string>(), It.IsAny<CancellationToken>()))
                           .ReturnsAsync(httpResponseMessage);

            var result = await CreateCommunicator().PostAsync<Request, Response>(request, default, CancellationToken.None);

            Assert.AreEqual(message, result.Message);
        }


        [Test]
        public void ThrowsBadRequestExceptionIfBadRequestReceivedOnPostAsync()
        {
            var errors = new[] {new ResponseError("type1", "message1"), new ResponseError("type2", "message2")};
            var badResponseMessage = new BadRequestResponse(errors);
            var json = JsonConvert.SerializeObject(badResponseMessage);
            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent(json)
            };
            _httpClientMock.Setup(client => client.PostAsJsonAsync(It.IsAny<Request>(), It.IsAny<string>(), It.IsAny<CancellationToken>()))
                           .ReturnsAsync(httpResponseMessage);

            Assert.ThrowsAsync<BadRequestException>(() => CreateCommunicator()
                                                        .PostAsync<Request, Response>(new Request(), default, CancellationToken.None));
        }

        [Test]
        public async Task ThrowsBadRequestExceptionWithAllInformationFromResponseIfBadRequestReceivedOnPostAsync()
        {
            var errors = new[] {new ResponseError("type1", "message1"), new ResponseError("type2", "message2")};
            var badResponseMessage = new BadRequestResponse(errors);
            var json = JsonConvert.SerializeObject(badResponseMessage);
            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent(json)
            };
            _httpClientMock.Setup(client => client.PostAsJsonAsync(It.IsAny<Request>(), It.IsAny<string>(), It.IsAny<CancellationToken>()))
                           .ReturnsAsync(httpResponseMessage);
            try
            {
                await CreateCommunicator().PostAsync<Request, Response>(new Request(), default, CancellationToken.None);
            }
            catch (BadRequestException e)
            {
                Assert.AreEqual("type1", e.Response.Errors.ElementAt(0).Type);
                Assert.AreEqual("message1", e.Response.Errors.ElementAt(0).Message);
                Assert.AreEqual("type2", e.Response.Errors.ElementAt(1).Type);
                Assert.AreEqual("message2", e.Response.Errors.ElementAt(1).Message);
                return;
            }

            Assert.Fail("Expected exception was not thrown");
        }

        [Test]
        public void ThrowsHttpRequestExceptionIfRequestNotSuccessfulOnPostAsync()
        {
            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.MovedPermanently)
            {
                Content = new StringContent("{}")
            };
            _httpClientMock.Setup(client => client.PostAsJsonAsync(It.IsAny<Request>(), It.IsAny<string>(), It.IsAny<CancellationToken>()))
                           .ReturnsAsync(httpResponseMessage);

            Assert.ThrowsAsync<HttpRequestException>(() => CreateCommunicator()
                                                         .PostAsync<Request, Response>(new Request(), default, CancellationToken.None));
        }

        [Test]
        public async Task CreateHttpClientWithCorrectBaseAddressOnGetAsync()
        {
            await CreateCommunicator().GetAsync<Response>(default, CancellationToken.None);

            _httpClientFactoryMock.Verify(factory => factory.GetOrCreate(_baseUri), Times.Once);
        }

        [TestCase("url/1")]
        [TestCase("url/2")]
        public async Task PassesAllParametersToHttpClientOnGetAsync(string path)
        {
            await CreateCommunicator().GetAsync<Response>(path, CancellationToken.None);

            _httpClientMock.Verify(client => client.GetAsync(path, CancellationToken.None), Times.Once);
        }

        [TestCase("expected message 1")]
        [TestCase("another expected message")]
        public async Task ReturnsSerializedResponseOnGetAsync(string message)
        {
            var response = new Response {Message = message};
            var json = JsonConvert.SerializeObject(response);
            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.Created)
            {
                Content = new StringContent(json)
            };
            _httpClientMock
                .Setup(client => client.GetAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(httpResponseMessage);

            var result = await CreateCommunicator().GetAsync<Response>(default, CancellationToken.None);

            Assert.AreEqual(message, result.Message);
        }

        [Test]
        public void ThrowsBadRequestExceptionIfBadRequestReceivedOnGetAsync()
        {
            var errors = new[] {new ResponseError("type1", "message1"), new ResponseError("type2", "message2")};
            var badResponseMessage = new BadRequestResponse(errors);
            var json = JsonConvert.SerializeObject(badResponseMessage);
            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent(json)
            };
            _httpClientMock
                .Setup(client => client.GetAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(httpResponseMessage);

            Assert.ThrowsAsync<BadRequestException>(() => CreateCommunicator().GetAsync<Response>(default, CancellationToken.None));
        }

        [Test]
        public async Task ThrowsBadRequestExceptionWithAllInformationFromResponseIfBadRequestReceivedOnGetAsync()
        {
            var errors = new[] {new ResponseError("type1", "message1"), new ResponseError("type2", "message2")};
            var badResponseMessage = new BadRequestResponse(errors);
            var json = JsonConvert.SerializeObject(badResponseMessage);
            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent(json)
            };
            _httpClientMock
                .Setup(client => client.GetAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(httpResponseMessage);
            try
            {
                await CreateCommunicator().GetAsync<Response>(default, CancellationToken.None);
            }
            catch (BadRequestException e)
            {
                Assert.AreEqual("type1", e.Response.Errors.ElementAt(0).Type);
                Assert.AreEqual("message1", e.Response.Errors.ElementAt(0).Message);
                Assert.AreEqual("type2", e.Response.Errors.ElementAt(1).Type);
                Assert.AreEqual("message2", e.Response.Errors.ElementAt(1).Message);
                return;
            }

            Assert.Fail("Expected exception was not thrown");
        }

        [Test]
        public void ThrowsHttpRequestExceptionIfRequestNotSuccessfulOnGetAsync()
        {
            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.MovedPermanently)
            {
                Content = new StringContent("{}")
            };
            _httpClientMock
                .Setup(client => client.GetAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(httpResponseMessage);

            Assert.ThrowsAsync<HttpRequestException>(() => CreateCommunicator().GetAsync<Response>(default, CancellationToken.None));
        }

        [Test]
        public async Task CreateHttpClientWithCorrectBaseAddressOnSubmitFormToStringAsync()
        {
            await CreateCommunicator().SubmitFormToStringAsync(new Dictionary<string, string>(), default, CancellationToken.None);

            _httpClientFactoryMock.Verify(factory => factory.GetOrCreate(_baseUri), Times.Once);
        }

        [TestCase("url/1")]
        [TestCase("url/2")]
        public async Task PassesAllParametersToHttpClientOnSubmitFormToStringAsync(string path)
        {
            var formData = new Dictionary<string, string>();
            await CreateCommunicator().SubmitFormToStringAsync(formData, path, CancellationToken.None);

            _httpClientMock.Verify(client => client.PostFormAsync(formData, path, CancellationToken.None), Times.Once);
        }

        [TestCase("expected message 1")]
        [TestCase("another expected message")]
        public async Task ReturnsSerializedResponseOnSubmitFormToStringAsync(string message)
        {
            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.Created)
            {
                Content = new StringContent(message)
            };
            _httpClientMock
                .Setup(client => client.PostFormAsync(It.IsAny<Dictionary<string, string>>(), 
                                                      It.IsAny<string>(),
                                                      It.IsAny<CancellationToken>()))
                .ReturnsAsync(httpResponseMessage);

            var result = await CreateCommunicator().SubmitFormToStringAsync(default, default, CancellationToken.None);

            Assert.AreEqual(message, result);
        }

        [Test]
        public void ThrowsBadRequestExceptionIfBadRequestReceivedOnSubmitFormToStringAsync()
        {
            var errors = new[] {new ResponseError("type1", "message1"), new ResponseError("type2", "message2")};
            var badResponseMessage = new BadRequestResponse(errors);
            var json = JsonConvert.SerializeObject(badResponseMessage);
            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent(json)
            };
            _httpClientMock
                .Setup(client => client.PostFormAsync(It.IsAny<Dictionary<string, string>>(),
                                                      It.IsAny<string>(),
                                                      It.IsAny<CancellationToken>()))
                .ReturnsAsync(httpResponseMessage);

            Assert.ThrowsAsync<BadRequestException>(() => CreateCommunicator()
                                                        .SubmitFormToStringAsync(default, default, CancellationToken.None));
        }

        [Test]
        public async Task ThrowsBadRequestExceptionWithAllInformationFromResponseIfBadRequestReceivedOnSubmitFormToStringAsync()
        {
            var errors = new[] {new ResponseError("type1", "message1"), new ResponseError("type2", "message2")};
            var badResponseMessage = new BadRequestResponse(errors);
            var json = JsonConvert.SerializeObject(badResponseMessage);
            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.BadRequest)
            {
                Content = new StringContent(json)
            };
            _httpClientMock
                .Setup(client => client.PostFormAsync(It.IsAny<Dictionary<string, string>>(),
                                                      It.IsAny<string>(),
                                                      It.IsAny<CancellationToken>()))
                .ReturnsAsync(httpResponseMessage);
            try
            {
                await CreateCommunicator().SubmitFormToStringAsync(default, default, CancellationToken.None);
            }
            catch (BadRequestException e)
            {
                Assert.AreEqual("type1", e.Response.Errors.ElementAt(0).Type);
                Assert.AreEqual("message1", e.Response.Errors.ElementAt(0).Message);
                Assert.AreEqual("type2", e.Response.Errors.ElementAt(1).Type);
                Assert.AreEqual("message2", e.Response.Errors.ElementAt(1).Message);
                return;
            }

            Assert.Fail("Expected exception was not thrown");
        }

        [Test]
        public void ThrowsHttpRequestExceptionIfRequestNotSuccessfulOnSubmitFormToStringAsync()
        {
            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.MovedPermanently)
            {
                Content = new StringContent("{}")
            };
            _httpClientMock
                .Setup(client => client.PostFormAsync(It.IsAny<Dictionary<string, string>>(), 
                                                      It.IsAny<string>(),
                                                      It.IsAny<CancellationToken>()))
                .ReturnsAsync(httpResponseMessage);

            Assert.ThrowsAsync<HttpRequestException>(() => CreateCommunicator()
                                                         .SubmitFormToStringAsync(default, default, CancellationToken.None));
        }


        private class Request
        {
        }

        private class Response
        {
            public string Message { get; set; }
        }
    }
}