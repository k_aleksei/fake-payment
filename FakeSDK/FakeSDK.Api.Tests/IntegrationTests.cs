using System;
using System.Threading;
using System.Threading.Tasks;
using FakeSDK.Api.Models;
using FakeSDK.Api.Services;
using Newtonsoft.Json;
using NUnit.Framework;

namespace FakeSDK.Api.Tests
{
    [TestFixture, Ignore("Made to be used only for development. Should no be run by continues deployment")]
    //[TestFixture]
    public class IntegrationTests
    {
        private ISecureFakePaymentServiceBuilder _secureFakePaymentServiceBuilder;

        [SetUp]
        public void SetUp()
        {
            _secureFakePaymentServiceBuilder = new SecureFakePaymentServiceBuilder().WithConfigurationForDemo();
        }

        [TearDown]
        public void Dispose()
        {
            _secureFakePaymentServiceBuilder.Dispose();
        }
        
        private ISecureFakePaymentService CreateService()
        {
           
            return _secureFakePaymentServiceBuilder.CreateService();
        }

        [Test]
        public async Task ReturnsExpectedTemplate()
        {
            var createPaymentRequest = new CreateSecurePaymentRequest()
            {
                Amount = 12312,
                CardHolder = "TEST TESTER",
                CardExpiryDate = DateTime.ParseExact("1123", "MMyy", null),
                CardNumber = "4111111111111111",
                Country = "CY",
                Currency = "USD",
                OrderId = "DBB99946-A10A-4D1B-A742-577FA026BC01",
                CVV = "111",
                Md = "testorder-1"
            };
            using var tokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(150));

            var response = await CreateService().CreatePaymentAndPrepareSecureHtmlAsync(createPaymentRequest, tokenSource.Token);
            
            Console.WriteLine(JsonConvert.SerializeObject(response));
            Assert.NotNull(response.ApproveHtmlTemplate);
        }
        
        [Test]
        public async Task ReturnsSecureHtml()
        {
            var createPaymentRequest = new CreateApprovedPaymentRequest()
            {
                Amount = 12312,
                CardHolder = "TEST TESTER",
                CardExpiryDate = DateTime.ParseExact("1123", "MMyy", null),
                CardNumber = "4111111111111111",
                Country = "CY",
                Currency = "USD",
                OrderId = "DBB99946-A10A-4D1B-A742-577FA026BC01",
                CVV = "111",
                Md = "testorder-1",
                CallbackUrl = "http://localhost"
            };
            using var tokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(150));

            var response = await CreateService().CreateApprovedPaymentAndGetSecureHtmlAsync(createPaymentRequest, tokenSource.Token);
            
            Console.WriteLine(JsonConvert.SerializeObject(response));
            Assert.NotNull(response.SecurePaymentHtmlForm);
        }
    }
}