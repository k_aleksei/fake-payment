using System;
using FakeSDK.Api.Infrastructure;
using FakeSDK.Api.ViewModels;
using NUnit.Framework;

namespace FakeSDK.Api.Tests.Infrastructure
{
    [TestFixture]
    public class ViewTemplateProviderIntegrationTests
    {
        [Test]
        public void ReturnsExpectedTemplate()
        {
            var viewModel = new CreatePaymentTemplateRazorViewModel("testorder-1", "MDAwMDAwMDAt");
            
            var template = new ViewTemplateProvider().GetCompiledCreatePaymentTemplate(viewModel);
            
            Console.WriteLine(template);
            Assert.NotNull(template);
        }
    }
}