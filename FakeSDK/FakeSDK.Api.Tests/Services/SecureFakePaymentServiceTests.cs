using System;
using System.Threading;
using System.Threading.Tasks;
using FakeSDK.Api.Common;
using FakeSDK.Api.Infrastructure;
using FakeSDK.Api.Models;
using FakeSDK.Api.Services;
using FakeSDK.Api.ViewModels;
using FakeSDK.Models;
using FakeSDK.Services;
using Moq;
using NUnit.Framework;

namespace FakeSDK.Api.Tests.Services
{
    [TestFixture]
    public class SecureFakePaymentServiceTests
    {
        [SetUp]
        public void SetUp()
        {
            _viewTemplateProviderMock = new Mock<IViewTemplateProvider>();
            _fakePaymentServiceMock = new Mock<IFakePaymentService>();
            SetupCreatePaymentResponse();
            SetupTemplate();
        }

        private Mock<IViewTemplateProvider> _viewTemplateProviderMock;
        private Mock<IFakePaymentService> _fakePaymentServiceMock;

        private SecureFakePaymentService CreateService()
        {
            return new SecureFakePaymentService(_viewTemplateProviderMock.Object, _fakePaymentServiceMock.Object);
        }

        private void SetupCreatePaymentResponse(string paReq = default, Guid transactionId = default)
        {
            var response = new CreatePaymentResponse(transactionId, default, paReq, default, default);
            _fakePaymentServiceMock
                .Setup(service => service.CreatePaymentAsync(It.IsAny<CreatePaymentRequest>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(response);
        }
        
        private void SetupConfirmationPage(string html = default)
        {
            _fakePaymentServiceMock
                .Setup(service => service.GetConfirmationPageHtmlAsync(It.IsAny<AutoApprovePaymentRequest>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(html);
        }
        
        private void SetupTemplate(string template = default)
        {
            _viewTemplateProviderMock
                .Setup(provider => provider.GetCompiledCreatePaymentTemplate(It.IsAny<CreatePaymentTemplateRazorViewModel>()))
                .Returns(template);
        }

        [Test]
        public async Task CreatesPayment_WithPassedParameters_OnCreatePaymentAndPrepareSecureHtmlAsync()
        {
            var request = new CreateSecurePaymentRequest();
            using var tokeSource = new CancellationTokenSource();
            var token = tokeSource.Token;

            await CreateService().CreatePaymentAndPrepareSecureHtmlAsync(request, token);

            _fakePaymentServiceMock.Verify(service => service.CreatePaymentAsync(request, token));
        }

        [TestCase("md-1", "paReq-1")]
        [TestCase("md-2", "paReq-2")]
        public async Task RendersCratePaymentTemplate_WithCombinedParameters_OnCreatePaymentAndPrepareSecureHtmlAsync(string md, string paReq)
        {
            var request = new CreateSecurePaymentRequest
            {
                Md = md
            };
            SetupCreatePaymentResponse(paReq);

            await CreateService().CreatePaymentAndPrepareSecureHtmlAsync(request, default);

            _viewTemplateProviderMock
                .Verify(provider => provider
                            .GetCompiledCreatePaymentTemplate(It.Is<CreatePaymentTemplateRazorViewModel>(m =>
                                                                  m.Md == md &&
                                                                  m.PaReq == paReq &&
                                                                  m.CallbackUrl == Constants.SecurePaymentCallbackPath)));
        }

        [TestCase("some template")]
        [TestCase("another template")]
        public async Task ReturnsTemplateString_ReturnedByProvider_OnCreatePaymentAndPrepareSecureHtmlAsync(string template)
        {
            SetupTemplate(template);

            var result = await CreateService().CreatePaymentAndPrepareSecureHtmlAsync(new CreateSecurePaymentRequest(), default);
            
            Assert.AreEqual(template, result.ApproveHtmlTemplate);
        }
        
        [Test]
        public async Task CreatesPayment_WithPassedParameters_OnCreateApprovedPaymentAndGetSecureHtmlAsync()
        {
            var request = new CreateApprovedPaymentRequest();
            using var tokeSource = new CancellationTokenSource();
            var token = tokeSource.Token;

            await CreateService().CreateApprovedPaymentAndGetSecureHtmlAsync(request, token);

            _fakePaymentServiceMock.Verify(service => service.CreatePaymentAsync(request, token));
        }
        
        [Test]
        public async Task ApprovesPayment_WithPassedParameters_OnCreateApprovedPaymentAndGetSecureHtmlAsync()
        {
            const string callbackUrl = "localhost";
            var transactionId = Guid.NewGuid();
            const string md = "tester-test01";
            var request = new CreateApprovedPaymentRequest
            {
                Md = md,
                CallbackUrl = callbackUrl
            };
            SetupCreatePaymentResponse(transactionId:transactionId);
            using var tokeSource = new CancellationTokenSource();
            var token = tokeSource.Token;

            await CreateService().CreateApprovedPaymentAndGetSecureHtmlAsync(request, token);

            _fakePaymentServiceMock.Verify(s => s.GetConfirmationPageHtmlAsync(It.Is<AutoApprovePaymentRequest>(r =>
                                                                                   r.CallbackUrl == callbackUrl &&
                                                                                   r.Md == md &&
                                                                                   r.TransactionId == transactionId.ToString()),
                                                                               token));
        }
        
        [Test]
        public async Task ReturnsResponseIncludingForm_FromService_OnCreateApprovedPaymentAndGetSecureHtmlAsync()
        {
            const string html = "<html/>";
            var request = new CreateApprovedPaymentRequest();
            SetupConfirmationPage(html);
            using var tokeSource = new CancellationTokenSource();
            var token = tokeSource.Token;

            var response = await CreateService().CreateApprovedPaymentAndGetSecureHtmlAsync(request, token);

            Assert.AreEqual(html, response.SecurePaymentHtmlForm);
        }
    }
}