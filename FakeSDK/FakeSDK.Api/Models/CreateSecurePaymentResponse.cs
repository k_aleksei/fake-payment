using System;
using FakeSDK.Models;

namespace FakeSDK.Api.Models
{
    public class CreateSecurePaymentResponse : CreatePaymentResponse
    {
        public CreateSecurePaymentResponse(
            string approveHtmlTemplate,
            Guid transactionId,
            string transactionStatus,
            string paReq,
            string url,
            string method)
            : base(transactionId, transactionStatus, paReq, url, method)
        {
            ApproveHtmlTemplate = approveHtmlTemplate;
        }
        public static CreateSecurePaymentResponse FromBaseResponse(CreatePaymentResponse baseResponse, string approveHtmlTemplate)
        {
            return new CreateSecurePaymentResponse(approveHtmlTemplate,
                                                   baseResponse.TransactionId,
                                                   baseResponse.TransactionStatus,
                                                   baseResponse.PaReq,
                                                   baseResponse.Url,
                                                   baseResponse.Method);
        }
        public string ApproveHtmlTemplate { get; }
    }
}