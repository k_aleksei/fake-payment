using FakeSDK.Models;

namespace FakeSDK.Api.Models
{
    public class CreateSecurePaymentRequest : CreatePaymentRequest
    {
        public string Md { get; set; }
    }
}