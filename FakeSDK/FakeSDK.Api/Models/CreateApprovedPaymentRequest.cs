namespace FakeSDK.Api.Models
{
    public class CreateApprovedPaymentRequest : CreateSecurePaymentRequest
    {
        public string CallbackUrl { get; set; }
    }
}