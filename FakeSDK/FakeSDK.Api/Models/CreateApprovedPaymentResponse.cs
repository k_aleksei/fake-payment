using System;
using FakeSDK.Models;

namespace FakeSDK.Api.Models
{
    public class CreateApprovedPaymentResponse : CreatePaymentResponse
    {
        public CreateApprovedPaymentResponse(
            string securePaymentHtmlForm,
            Guid transactionId,
            string transactionStatus,
            string paReq,
            string url,
            string method)
            : base(transactionId, transactionStatus, paReq, url, method)
        {
            SecurePaymentHtmlForm = securePaymentHtmlForm;
        }

        public static CreateApprovedPaymentResponse FromBaseResponse(CreatePaymentResponse baseResponse, string securePaymentHtmlForm)
        {
            return new CreateApprovedPaymentResponse(securePaymentHtmlForm,
                                                     baseResponse.TransactionId,
                                                     baseResponse.TransactionStatus,
                                                     baseResponse.PaReq,
                                                     baseResponse.Url,
                                                     baseResponse.Method);
        }
        public string SecurePaymentHtmlForm { get; }
    }
}