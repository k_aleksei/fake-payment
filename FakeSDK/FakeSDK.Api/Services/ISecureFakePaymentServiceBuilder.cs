using System;

namespace FakeSDK.Api.Services
{
    public interface ISecureFakePaymentServiceBuilder : IDisposable
    {
        ISecureFakePaymentServiceBuilder WithConfigurationForDemo();
        ISecureFakePaymentServiceBuilder WithConfiguration(string merchantId, string secretKey);
        ISecureFakePaymentService CreateService();
    }

}