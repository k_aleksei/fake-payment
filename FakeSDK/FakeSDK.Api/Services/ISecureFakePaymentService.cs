using System.Threading;
using System.Threading.Tasks;
using FakeSDK.Api.Models;

namespace FakeSDK.Api.Services
{
    public interface ISecureFakePaymentService
    {
        Task<CreateSecurePaymentResponse> CreatePaymentAndPrepareSecureHtmlAsync(
            CreateSecurePaymentRequest request,
            CancellationToken cancellationToken);

        Task<CreateApprovedPaymentResponse> CreateApprovedPaymentAndGetSecureHtmlAsync(
            CreateApprovedPaymentRequest request,
            CancellationToken token);
    }
}