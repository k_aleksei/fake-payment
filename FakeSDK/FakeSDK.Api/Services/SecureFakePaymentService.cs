using System.Threading;
using System.Threading.Tasks;
using FakeSDK.Api.Infrastructure;
using FakeSDK.Api.Models;
using FakeSDK.Api.ViewModels;
using FakeSDK.Models;
using FakeSDK.Services;
using Serilog;

namespace FakeSDK.Api.Services
{
    internal class SecureFakePaymentService : ISecureFakePaymentService
    {
        private readonly IFakePaymentService _fakePaymentService;
        private readonly IViewTemplateProvider _templateProvider;

        public SecureFakePaymentService(IViewTemplateProvider templateProvider, IFakePaymentService fakePaymentService)
        {
            _templateProvider = templateProvider;
            _fakePaymentService = fakePaymentService;
        }

        public async Task<CreateSecurePaymentResponse> CreatePaymentAndPrepareSecureHtmlAsync(CreateSecurePaymentRequest request, CancellationToken token)
        {
            Log.Information("new secured payment request started: {@CreateSecurePaymentRequest}", request);
            var createPaymentResponse = await _fakePaymentService.CreatePaymentAsync(request, token);
            var viewModel = new CreatePaymentTemplateRazorViewModel(request.Md, createPaymentResponse.PaReq);
            var approvePaymentTemplate = _templateProvider.GetCompiledCreatePaymentTemplate(viewModel);
            return CreateSecurePaymentResponse.FromBaseResponse(createPaymentResponse, approvePaymentTemplate);
        }

        public async Task<CreateApprovedPaymentResponse> CreateApprovedPaymentAndGetSecureHtmlAsync(CreateApprovedPaymentRequest request, CancellationToken token)
        {
            Log.Information("new approved payment request started: {@CreateApprovedPaymentRequest}", request);
            var createPaymentResponse = await _fakePaymentService.CreatePaymentAsync(request, token);
           
            var approvePaymentRequest = new AutoApprovePaymentRequest
            {
                CallbackUrl = request.CallbackUrl,
                Md = request.Md, 
                TransactionId = createPaymentResponse.TransactionId.ToString()
            };
            var securePaymentHtmlForm = await _fakePaymentService.GetConfirmationPageHtmlAsync(approvePaymentRequest, token);
            return CreateApprovedPaymentResponse.FromBaseResponse(createPaymentResponse, securePaymentHtmlForm);
        }
    }
}