using FakeSDK.Api.Infrastructure;
using FakeSDK.Services;

namespace FakeSDK.Api.Services
{
    public class SecureFakePaymentServiceBuilder : ISecureFakePaymentServiceBuilder
    {
        private readonly FakePaymentServiceBuilder _paymentServiceBuilder;
        private readonly ViewTemplatesProviderFactory _templateProviderFactory;

        public SecureFakePaymentServiceBuilder()
        {
            _templateProviderFactory = new ViewTemplatesProviderFactory();
            _paymentServiceBuilder = new FakePaymentServiceBuilder();
        }

        public ISecureFakePaymentServiceBuilder WithConfigurationForDemo()
        {
            _paymentServiceBuilder.WithConfigurationForDemo();
            return this;
        }

        public ISecureFakePaymentServiceBuilder WithConfiguration(string merchantId, string secretKey)
        {
            _paymentServiceBuilder.WithConfiguration(merchantId, secretKey);
            return this;
        }

        public ISecureFakePaymentService CreateService()
        {
            return new SecureFakePaymentService(_templateProviderFactory.Create(), _paymentServiceBuilder.Build());
        }

        public void Dispose()
        {
            _paymentServiceBuilder.Dispose();
        }
    }
}