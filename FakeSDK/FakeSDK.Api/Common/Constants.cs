namespace FakeSDK.Api.Common
{
    internal static class Constants
    {
        public static string SecurePaymentPath { get; } = "http://dumdumpay.site/secure/";
        public static string SecurePaymentSubmittingPath { get; } = "http://dumdumpay.site/secure/submit";
        public static string SecurePaymentCallbackPath { get; } = "http://localhost:5000/fake-payment/created";
        public static string CreatePaymentTemplateResourcePath { get; } = "Views.CreatePaymentTemplate";
    }
}