using System;

namespace FakeSDK.Api.Infrastructure.Exceptions
{
    public class SecurePaymentException : Exception
    {
        public SecurePaymentException(string error) : base(error)
        {
        }    
        public SecurePaymentException(string error, Exception exception) : base(error, exception)
        {
        }
    }
}