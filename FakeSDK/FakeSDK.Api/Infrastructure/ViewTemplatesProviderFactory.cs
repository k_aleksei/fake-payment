namespace FakeSDK.Api.Infrastructure
{
    internal class ViewTemplatesProviderFactory : IViewTemplatesProviderFactory
    {
        public IViewTemplateProvider Create()
        {
            return new ViewTemplateProvider();
        }
    }
}