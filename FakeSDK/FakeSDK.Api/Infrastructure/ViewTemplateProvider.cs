using System;
using FakeSDK.Api.Common;
using FakeSDK.Api.Infrastructure.Exceptions;
using FakeSDK.Api.ViewModels;
using RazorEngine.Configuration;
using RazorEngine.Templating;

namespace FakeSDK.Api.Infrastructure
{
    internal class ViewTemplateProvider : IViewTemplateProvider
    {
        public string GetCompiledCreatePaymentTemplate(CreatePaymentTemplateRazorViewModel viewModel)
        {
            try
            {
                var config = new TemplateServiceConfiguration
                {
                    TemplateManager = new EmbeddedResourceTemplateManager(typeof(ModuleRoot))
                };

                using var service = RazorEngineService.Create(config);
                return service.RunCompile(Constants.CreatePaymentTemplateResourcePath, model: viewModel);
            }
            catch (Exception e)
            {
                throw new SecurePaymentException("Unable to compile create payment template", e);
            }
        }
    }
}