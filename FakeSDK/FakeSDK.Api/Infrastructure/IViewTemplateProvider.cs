using FakeSDK.Api.ViewModels;

namespace FakeSDK.Api.Infrastructure
{
    public interface IViewTemplateProvider
    {
        string GetCompiledCreatePaymentTemplate(CreatePaymentTemplateRazorViewModel viewModel);
    }
}