using System.Threading.Tasks;

namespace FakeSDK.Api.Infrastructure
{
    internal interface IEmbeddedResourcesProvider
    {
        Task<string> GetResourceOrDefault(string path);
    }
}