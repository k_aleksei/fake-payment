namespace FakeSDK.Api.Infrastructure
{
    public interface IViewTemplatesProviderFactory
    {
        IViewTemplateProvider Create();
    }
}