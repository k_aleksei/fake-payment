using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace FakeSDK.Api.Infrastructure
{
    internal class EmbeddedResourcesProvider : IEmbeddedResourcesProvider
    {
        public async Task<string> GetResourceOrDefault(string path)
        {
            var assembly = typeof(EmbeddedResourcesProvider).Assembly;
            var resource = assembly.GetManifestResourceStream(path);

            if(resource == null)
            {
                return default;
            }

            using var reader = new StreamReader(resource, Encoding.UTF8, true, 1024, true);
            return await reader.ReadToEndAsync();
        }
    }
}