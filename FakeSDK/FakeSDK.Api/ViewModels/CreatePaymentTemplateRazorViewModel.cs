using FakeSDK.Api.Common;

namespace FakeSDK.Api.ViewModels
{
    public class CreatePaymentTemplateRazorViewModel
    {
        public CreatePaymentTemplateRazorViewModel(string md, string paReq)
        {
            Md = md;
            PaReq = paReq;
            CallbackUrl = Constants.SecurePaymentCallbackPath;
        }

        public string Md { get; set; }
        public string PaReq { get; set; }
        public string CallbackUrl { get; set; }
    }
}