using System;

namespace FakeSDK.Services
{
    public interface IFakePaymentServiceBuilder : IDisposable
    {
        IFakePaymentServiceBuilder WithConfigurationForDemo();
        IFakePaymentServiceBuilder WithConfiguration(string merchantId, string secretKey);
        IFakePaymentService Build();
    }
}