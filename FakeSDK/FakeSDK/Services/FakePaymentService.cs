using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FakeSDK.Common;
using FakeSDK.Infrastructure;
using FakeSDK.Infrastructure.Exceptions;
using FakeSDK.Models;
using Serilog;

namespace FakeSDK.Services
{
    internal sealed class FakePaymentService : IFakePaymentService
    {
        private readonly IHttpCommunicator _operationsCommunicator;
        private readonly IHttpCommunicator _approvalCommunicator;

        public FakePaymentService(IHttpCommunicatorFactory httpCommunicatorFactory)
        {
            _operationsCommunicator = httpCommunicatorFactory.CreateOperationsCommunicator();
            _approvalCommunicator = httpCommunicatorFactory.CreateApprovalCommunicator();
        }

        public async Task<CreatePaymentResponse> CreatePaymentAsync(
            CreatePaymentRequest request,
            CancellationToken token)
        {
            Log.Information("new create payment request started: {@CreatePaymentRequest}", request);
            try
            {
                var responseInfo = await _operationsCommunicator
                    .PostAsync<CreatePaymentRequest, CreatePaymentResponseInfo>(request, Constants.CreatePath, token);
                
                Log.Information("new payment created: {@CreatePaymentResponse}", responseInfo.Result);
                return responseInfo.Result;
            }
            catch (BadRequestException e)
            {
                Log.Error(e, "[{@orderId}] new payment creation failed", request.OrderId);
                throw new CreatePaymentException(e);
            }
            catch (Exception e)
            {
                Log.Error(e, "[{@orderId}] new payment creation failed", request.OrderId);
                var error = new ResponseError(Constants.Errors.CreateFailedErrorType, e.Message);
                var response = new BadRequestResponse(new[] {error});
                throw new CreatePaymentException(response, e);
            }
        }

        public async Task<ConfirmPaymentResponse> ConfirmPaymentAsync(ConfirmPaymentRequest request, CancellationToken token)
        {
            try
            {
                Log.Information("new confirm payment request started: {@CreatePaymentRequest}", request);
                
                var result = await _operationsCommunicator
                    .PostAsync<ConfirmPaymentRequest, ConfirmPaymentResponse>(request, Constants.ConfirmPath, token);
               
                Log.Information("new payment confirmed: {@CreatePaymentResponse}", result);
                return result;
            }
            catch (BadRequestException e)
            {
                Log.Error(e, "[{@transactionId}] new payment confirmation failed", request.TransactionId);
                throw new ConfirmPaymentException(e);
            }
            catch (Exception e)
            {
                Log.Error(e, "[{@transactionId}] new payment confirmation failed", request.TransactionId);
                var error = new ResponseError(Constants.Errors.ConfirmFailedErrorType, e.Message);
                var response = new BadRequestResponse(new[] {error});
                throw new ConfirmPaymentException(response, e);
            }
        }

        public async Task<PaymentStatusResponse> GetPaymentStatusAsync(Guid transactionId, CancellationToken token)
        {
            try
            {
                var path = string.Format(Constants.PaymentStatusPathTemplate, transactionId);
                return await _operationsCommunicator.GetAsync<PaymentStatusResponse>(path, token);
            }
            catch (BadRequestException e)
            {
                Log.Error(e, "[{@transactionId}] get payment status failed", transactionId);
                throw new GetPaymentStatusException(e);
            }
            catch (Exception e)
            {
                Log.Error(e, "[{@transactionId}] get payment status failed", transactionId);
                var error = new ResponseError(Constants.Errors.GetPaymentStatusFailedErrorType, e.Message);
                var response = new BadRequestResponse(new[] {error});
                throw new GetPaymentStatusException(response, e);
            }
        }

        public async Task<string> GetConfirmationPageHtmlAsync(AutoApprovePaymentRequest request, CancellationToken token)
        {
            try
            {
                var formData = new Dictionary<string, string>
                {
                    {Constants.ApprovePaymentParameters.Md, request.Md},
                    {Constants.ApprovePaymentParameters.CallbackUrl, request.CallbackUrl},
                    {Constants.ApprovePaymentParameters.TransactionId, request.TransactionId},
                };
                
                return await _approvalCommunicator.SubmitFormToStringAsync(formData, Constants.ApprovePaymentPath, token);
            }
            catch (BadRequestException e)
            {
                Log.Error(e, "[{@transactionId}] approve payment status failed", request.TransactionId);
                throw new AutoApprovePaymentException(e);
            }
            catch (Exception e)
            {
                Log.Error(e, "[{@transactionId}] approve payment status failed", request.TransactionId);
                var error = new ResponseError(Constants.Errors.ApproveFailedErrorType, e.Message);
                var response = new BadRequestResponse(new[] {error});
                throw new AutoApprovePaymentException(response, e);
            }
        }
    }
}