using FakeSDK.Infrastructure;
using FakeSDK.Infrastructure.Configuration;
using FakeSDK.Infrastructure.Exceptions;

namespace FakeSDK.Services
{
    public class FakePaymentServiceBuilder : IFakePaymentServiceBuilder
    {
        private HttpClientFactory _httpClientFactory;
        private IHttpCommunicatorFactory _httpCommunicatorFactory;
        private FakePaymentConfiguration _configuration;

        public IFakePaymentServiceBuilder WithConfigurationForDemo()
        {
            Initialize("6fc3aa31-7afd-4df1-825f-192e60950ca1", "53cr3t");
            return this;
        }

        public IFakePaymentServiceBuilder WithConfiguration(string merchantId, string secretKey)
        {
            Initialize(merchantId, secretKey);
            return this;
        }

        private void Initialize(string merchantId, string secretKey)
        {
            _configuration = new FakePaymentConfiguration(merchantId, secretKey);
            _httpClientFactory = new HttpClientFactory(_configuration);
            _httpCommunicatorFactory = new HttpCommunicatorFactory(_httpClientFactory);
        }

        public IFakePaymentService Build()
        {
            if(_configuration == null) throw new PaymentConfigurationException();
            return new FakePaymentService(_httpCommunicatorFactory);
        }

        public void Dispose()
        {
            _httpClientFactory.Dispose();
        }
    }
}