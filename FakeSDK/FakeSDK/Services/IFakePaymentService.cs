using System;
using System.Threading;
using System.Threading.Tasks;
using FakeSDK.Models;

namespace FakeSDK.Services
{
    public interface IFakePaymentService
    {
        Task<CreatePaymentResponse> CreatePaymentAsync(CreatePaymentRequest request, CancellationToken token);

        Task<ConfirmPaymentResponse> ConfirmPaymentAsync(ConfirmPaymentRequest request, CancellationToken token);
        Task<PaymentStatusResponse> GetPaymentStatusAsync(Guid transactionId, CancellationToken token);
        Task<string> GetConfirmationPageHtmlAsync(AutoApprovePaymentRequest request, CancellationToken token);
    }
}