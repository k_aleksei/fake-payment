namespace FakeSDK.Infrastructure.Configuration
{
    public interface IFakePaymentConfiguration
    {
        public string MerchantId { get; }
        public string SecretKey { get; }
    }
}