using System;

namespace FakeSDK.Infrastructure.Configuration
{
    public class FakePaymentConfiguration: IFakePaymentConfiguration
    {
        public FakePaymentConfiguration(string merchantId, string secretKey)
        {
            MerchantId = merchantId ?? throw new ArgumentNullException(nameof(merchantId));
            SecretKey = secretKey ?? throw new ArgumentNullException(nameof(secretKey));
        }
        public string MerchantId { get; }
        public string SecretKey { get; }
    }
}