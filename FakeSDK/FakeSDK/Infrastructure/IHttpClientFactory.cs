using System;

namespace FakeSDK.Infrastructure
{
    internal interface IHttpClientFactory : IDisposable
    {
        IHttpClient GetOrCreate(Uri baseAddress);
    }
}