namespace FakeSDK.Infrastructure
{
    internal interface IHttpCommunicatorFactory
    {
        IHttpCommunicator CreateOperationsCommunicator();
        IHttpCommunicator CreateApprovalCommunicator();
    }
}