using Newtonsoft.Json.Converters;

namespace FakeSDK.Infrastructure.JsonConverters
{
    public class DateFormatConverter : IsoDateTimeConverter
    {
        public DateFormatConverter(string format)
        {
            DateTimeFormat = format;
        }
    }
}