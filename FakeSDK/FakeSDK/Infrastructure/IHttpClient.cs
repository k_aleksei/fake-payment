using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace FakeSDK.Infrastructure
{
    internal interface IHttpClient
    {
        Task<HttpResponseMessage> PostAsJsonAsync<TReq>(TReq requestReq, string path, CancellationToken token);
        Task<HttpResponseMessage> GetAsync(string path, CancellationToken token);
        Task<HttpResponseMessage> PostFormAsync(Dictionary<string, string> formData, string path, CancellationToken token);
    }
}