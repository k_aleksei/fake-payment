using FakeSDK.Common;

namespace FakeSDK.Infrastructure
{
    internal class HttpCommunicatorFactory : IHttpCommunicatorFactory
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public HttpCommunicatorFactory(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public IHttpCommunicator CreateOperationsCommunicator()
        {
            return new HttpCommunicator(_httpClientFactory, Constants.FakePaymentBaseUrl);
        }

        public IHttpCommunicator CreateApprovalCommunicator()
        {
            return new HttpCommunicator(_httpClientFactory, Constants.ApprovePaymentBaseUrl);
        }
    }
}