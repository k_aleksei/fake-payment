using System;

namespace FakeSDK.Infrastructure.Exceptions
{
    public class AutoApprovePaymentException : PaymentExceptionBase
    {
        public AutoApprovePaymentException(BadRequestException exception) : base(exception)
        {
        }

        public AutoApprovePaymentException(BadRequestResponse error, Exception exception) : base(error, exception)
        {
        }
    }
}