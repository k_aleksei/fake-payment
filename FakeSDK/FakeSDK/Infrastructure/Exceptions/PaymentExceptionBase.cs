using System;
using Newtonsoft.Json;

namespace FakeSDK.Infrastructure.Exceptions
{
    public abstract class PaymentExceptionBase : Exception
    {
        protected PaymentExceptionBase(BadRequestException exception) : base(exception.Message, exception)
        {
            Response = exception.Response;
        }

        protected PaymentExceptionBase(BadRequestResponse error, Exception exception)
            : base(JsonConvert.SerializeObject(error), exception)
        {
            Response = error;
        }

        public BadRequestResponse Response { get; }
    }
}