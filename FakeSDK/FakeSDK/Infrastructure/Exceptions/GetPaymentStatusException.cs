using System;

namespace FakeSDK.Infrastructure.Exceptions
{
    public class GetPaymentStatusException : PaymentExceptionBase
    {
        public GetPaymentStatusException(BadRequestException exception) : base(exception)
        {
        }

        public GetPaymentStatusException(BadRequestResponse error, Exception exception) : base(error, exception)
        {
        }
    }
}