using System;
using FakeSDK.Common;

namespace FakeSDK.Infrastructure.Exceptions
{
    public class PaymentConfigurationException : Exception
    {
        public PaymentConfigurationException() : base(Constants.Errors.ConfigurationError)
        {
        }
    }
}