using System;
using Newtonsoft.Json;

namespace FakeSDK.Infrastructure.Exceptions
{
    public class BadRequestException : Exception
    {
        public BadRequestException(string error) : base(error)
        {
            Response = JsonConvert.DeserializeObject<BadRequestResponse>(error);
        }
        public BadRequestResponse Response { get; }
    }
}