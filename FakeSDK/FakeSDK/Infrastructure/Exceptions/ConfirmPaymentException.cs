using System;

namespace FakeSDK.Infrastructure.Exceptions
{
    public class ConfirmPaymentException : PaymentExceptionBase
    {
        public ConfirmPaymentException(BadRequestException exception) : base(exception)
        {
        }

        public ConfirmPaymentException(BadRequestResponse error, Exception exception) : base(error, exception)
        {
        }
    }
}