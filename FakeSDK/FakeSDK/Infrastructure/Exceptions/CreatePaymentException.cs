using System;

namespace FakeSDK.Infrastructure.Exceptions
{
    public class CreatePaymentException : PaymentExceptionBase
    {
        public CreatePaymentException(BadRequestException exception) : base(exception)
        {
        }

        public CreatePaymentException(BadRequestResponse error, Exception exception) : base(error, exception)
        {
        }
    }
}