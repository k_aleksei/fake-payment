using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using FakeSDK.Infrastructure.Exceptions;
using Newtonsoft.Json;

namespace FakeSDK.Infrastructure
{
    internal class HttpCommunicator : IHttpCommunicator
    {
        private readonly Uri _baseAddress;
        private readonly IHttpClientFactory _httpClientFactory;

        public HttpCommunicator(IHttpClientFactory httpClientFactory, Uri baseAddress)
        {
            _httpClientFactory = httpClientFactory;
            _baseAddress = baseAddress;
        }

        public async Task<TRes> PostAsync<TReq, TRes>(TReq request, string path, CancellationToken cancellationToken)
        {
            var client = _httpClientFactory.GetOrCreate(_baseAddress);
            var response = await client.PostAsJsonAsync(request, path, cancellationToken);
            return await ProcessResponse<TRes>(response);
        }

        public async Task<TRes> GetAsync<TRes>(string path, CancellationToken cancellationToken)
        {
            var client = _httpClientFactory.GetOrCreate(_baseAddress);
            var response = await client.GetAsync(path, cancellationToken);
            return await ProcessResponse<TRes>(response);
        }

        public async Task<string> SubmitFormToStringAsync(
            Dictionary<string, string> formData,
            string path,
            CancellationToken cancellationToken)
        {
            var client = _httpClientFactory.GetOrCreate(_baseAddress);
            var response = await client.PostFormAsync(formData, path, cancellationToken);
            await ThrowExceptionIfRequestFailed(response);
            return await response.Content.ReadAsStringAsync();
        }

        private static async Task ThrowExceptionIfRequestFailed(HttpResponseMessage response)
        {
            if(response.StatusCode == HttpStatusCode.BadRequest)
            {
                var responseBody = await response.Content.ReadAsStringAsync();
                throw new BadRequestException(responseBody);
            }

            response.EnsureSuccessStatusCode();
        }

        private static async Task<TRes> ProcessResponse<TRes>(HttpResponseMessage response)
        {
            await ThrowExceptionIfRequestFailed(response);
            var responseBody = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<TRes>(responseBody);
        }
    }
}