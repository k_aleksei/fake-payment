using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace FakeSDK.Infrastructure
{
    internal interface IHttpCommunicator
    {
        Task<TRes> PostAsync<TReq, TRes>(TReq request, string path, CancellationToken cancellationToken);
        Task<TRes> GetAsync<TRes>(string path, CancellationToken cancellationToken);
        Task<string> SubmitFormToStringAsync(Dictionary<string, string> formData, string path, CancellationToken cancellationToken);
    }
}