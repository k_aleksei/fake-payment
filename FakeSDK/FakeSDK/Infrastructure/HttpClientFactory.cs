using System;
using System.Collections.Concurrent;
using System.Net.Http;
using FakeSDK.Infrastructure.Configuration;

namespace FakeSDK.Infrastructure
{
    internal class HttpClientFactory : IHttpClientFactory
    {
        private readonly IFakePaymentConfiguration _configuration;

        private readonly ConcurrentDictionary<Uri, HttpClient> _httpClients =
            new ConcurrentDictionary<Uri, HttpClient>();
        
        public HttpClientFactory(IFakePaymentConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IHttpClient GetOrCreate(Uri baseAddress)
        {
            var client = _httpClients.GetOrAdd(baseAddress, CreateClient);
            return new HttpClientDecorator(client);
        }

        private HttpClient CreateClient(Uri baseAddress)
        {
            var client = new HttpClient {BaseAddress = baseAddress};
            client.DefaultRequestHeaders.TryAddWithoutValidation("Mechant-Id", _configuration.MerchantId);
            client.DefaultRequestHeaders.TryAddWithoutValidation("Secret-Key", _configuration.SecretKey);
            return client;
        }
        
        public void Dispose()
        {
            foreach(var keyValuePair in _httpClients)
            {
                keyValuePair.Value.Dispose();
            }
        }
    }
}