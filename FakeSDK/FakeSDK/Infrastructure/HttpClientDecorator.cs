using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace FakeSDK.Infrastructure
{
    internal class HttpClientDecorator : IHttpClient
    {
        private readonly HttpClient _client;

        public HttpClientDecorator(HttpClient client)
        {
            _client = client;
        }

        public Task<HttpResponseMessage> PostAsJsonAsync<TReq>(TReq request, string path, CancellationToken token)
        {
            var data = JsonConvert.SerializeObject(request);
            var content = new StringContent(data, Encoding.UTF8, "application/json");
            return _client.PostAsync(path, content, token);
        }

        public Task<HttpResponseMessage> GetAsync(string path, CancellationToken token)
        {
            return _client.GetAsync(path, token);
        }

        public Task<HttpResponseMessage> PostFormAsync(Dictionary<string, string> formData,string path, CancellationToken token)
        {
            return _client.PostAsync(path, new FormUrlEncodedContent(formData), token);
        }
    }
}