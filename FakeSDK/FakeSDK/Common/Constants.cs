using System;
using FakeSDK.Infrastructure.Configuration;

namespace FakeSDK.Common
{
    internal static class Constants
    {
        public static readonly Uri FakePaymentBaseUrl = new Uri("https://private-anon-45ca887379-dumdumpay.apiary-proxy.com/api/");
        public static readonly Uri ApprovePaymentBaseUrl = new Uri("http://dumdumpay.site/");
        public const string CreatePath = "payment/create";
        public const string ConfirmPath = "payment/confirm";
        public const string PaymentStatusPathTemplate = "payment/{0}/status";
        public const string ApprovePaymentPath = "secure";
        
        public static class ApprovePaymentParameters
        {
            public const string Md = "MD";
            public const string CallbackUrl = "TermUrl";
            public const string TransactionId = "TransactionId";
        }

        public static class Errors
        {
            public const string CreateFailedErrorType = "Payment creation failed";
            public const string ConfirmFailedErrorType = "Payment confirmation failed";
            public const string GetPaymentStatusFailedErrorType = "Payment status retrieval error";
            public const string ApproveFailedErrorType = "Payment status approval error";
            public static readonly string ConfigurationError =
                $"Fake Payment not configured. [{nameof(IFakePaymentConfiguration.MerchantId)} and [{nameof(IFakePaymentConfiguration.SecretKey)} should be specified]";
        }
        
    }
}