namespace FakeSDK.Models
{
    public class AutoApprovePaymentRequest
    {
        public string CallbackUrl { get; set; }
        public string Md { get; set; }
        public string TransactionId { get; set; }
    }
}