using System;
using Newtonsoft.Json;

namespace FakeSDK.Models
{
    public class CreatePaymentResponse
    {
        [JsonConstructor]
        public CreatePaymentResponse(
            Guid transactionId,
            string transactionStatus,
            string paReq,
            string url,
            string method)
        {
            TransactionId = transactionId;
            TransactionStatus = transactionStatus;
            PaReq = paReq;
            Url = url;
            Method = method;
        }

        public Guid TransactionId { get; }
        public string TransactionStatus { get; }
        public string PaReq { get; }
        public string Url { get; }
        public string Method { get; }
    }
}