using System;
using Newtonsoft.Json;

namespace FakeSDK.Models
{
    public class PaymentInfo
    {
        [JsonConstructor]
        public PaymentInfo(Guid transactionId, PaymentStatus status, int amount, string currency, Guid orderId, string lastFourDigits)
        {
            TransactionId = transactionId;
            Status = status;
            Amount = amount;
            Currency = currency;
            OrderId = orderId;
            LastFourDigits = lastFourDigits;
        }

        public Guid TransactionId { get; }
        public PaymentStatus Status { get; }
        public int Amount { get; }
        public string Currency { get; }
        public Guid OrderId { get; }
        public string LastFourDigits { get; }
    }
}