using Newtonsoft.Json;

namespace FakeSDK.Models
{
    public class PaymentStatusResponse
    {
        [JsonConstructor]
        public PaymentStatusResponse(PaymentInfo result)
        {
            Result = result;
        }
        public PaymentInfo Result { get; }
    }
}