namespace FakeSDK.Models
{
    public class CreatePaymentResponseInfo
    {
        public CreatePaymentResponseInfo(CreatePaymentResponse result)
        {
            Result = result;
        }

        public CreatePaymentResponse Result { get; }
    }
}