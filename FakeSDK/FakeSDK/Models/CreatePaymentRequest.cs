using System;
using FakeSDK.Infrastructure.JsonConverters;
using Newtonsoft.Json;

namespace FakeSDK.Models
{
    public class CreatePaymentRequest
    {
        /// <summary>
        /// Order identifier in merchant's system
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// Amount to process. Should be integer. If you want to deposit 123.12, you should send 12312
        /// </summary>
        public uint Amount { get; set; }
        /// <summary>
        /// Valid currency ISO string code (not validated in this API)
        /// </summary>
        public string Currency { get; set; }
        /// <summary>
        /// Valid country ISO string code (not validated in this API)
        /// </summary>
        public string Country { get; set; }
        /// <summary>
        /// Valid credit card number (LUHN)
        /// </summary>
        public string CardNumber { get; set; }
        /// <summary>
        /// Credit card holder full name
        /// </summary>
        public string CardHolder { get; set; }
        /// <summary>
        /// Credit card expiry date in MMyy format
        /// </summary>
        [JsonConverter(typeof(DateFormatConverter), "MMyy")]
        public DateTime CardExpiryDate { get; set; }
        /// <summary>
        /// 3 symbol secure code
        /// </summary>
        public string CVV { get; set; }
    }
}