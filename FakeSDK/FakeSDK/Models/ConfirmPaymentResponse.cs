using Newtonsoft.Json;

namespace FakeSDK.Models
{
    public class ConfirmPaymentResponse
    {
        [JsonConstructor]
        public ConfirmPaymentResponse(PaymentInfo result)
        {
            Result = result;
        }

        public PaymentInfo Result { get; }
    }
}