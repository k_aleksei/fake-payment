namespace FakeSDK.Models
{
    public enum PaymentStatus
    {
        Init,
        Pending,
        Approved,
        Declined,
        DeclinedDueToInvalidCreditCard
    }
}