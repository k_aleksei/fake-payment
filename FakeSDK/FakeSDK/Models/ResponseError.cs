using Newtonsoft.Json;

public class ResponseError
{
    [JsonConstructor]
    public ResponseError(string type, string message)
    {
        Type = type;
        Message = message;
    }

    public string Type { get; }
    public string Message { get; }
}