using System.Collections.Generic;
using Newtonsoft.Json;

public class BadRequestResponse
{
    [JsonConstructor]
    public BadRequestResponse(IEnumerable<ResponseError> errors)
    {
        Errors = errors;
    }

    public IEnumerable<ResponseError> Errors { get; } 
}