#!/bin/bash
export ASPNETCORE_ENVIRONMENT=Development
SamplePath=FakeSDK.MVCSample/FakeSDK.MVCSample/

echo ========================================================
echo Redirect to MVC Sample directory
echo ========================================================
cd $SamplePath

echo ========================================================
echo Start MVC Sample
echo ========================================================
dotnet restore
dotnet build
dotnet run